# 深入浅出ASP.NET Core 视频课程的配套源代码

# 介绍

《从零开始学 Asp.net Core MVC》 视频课程的源代码库。

在本课程中，我们将学习从入门最基本的命令，到中级和高级的 ASP.NET Core 概念，也将从零开始创建一个 ASP.NET Core 项目。
在我们搭建这个项目并完成整个课程的过程中，我们将学习如何使用 ASP.NET Core Web 框架,并且能够掌握如何搭建以数据驱动的 Web 程序的一切知识。

我们的课程内容会包含：

- ASP.NET CORE 的基础知识
- ASP.NET CORE MVC 的详细说明
- Entity Framework Core 的必备知识
- ASP.NET Identity Core 我们将学习标准的 身份的认证以及授权和权限管理

  通过完成本课程，您将能够使用 ASP.NET Core 开发新的 Web 应用程序，并在为新项目选择技术时做出战略决策。

## 视频课程地址
 
前64章学习地址：https://www.ixigua.com/6823379956358185479?id=6740592107225874958


完整课程地址：https://www.52abp.com/College

配套图文：http://www.yoyomooc.com/

## 交流提问

> 交流群：
- 点击链接加入群聊【微软MVP带你学ASP.NET CORE】952387474：https://jq.qq.com/?_wv=1027&k=5rOxIoS

- 点击链接加入群聊【52ABP .NET CORE 实战交流】633751348：https://jq.qq.com/?_wv=1027&k=5EHBATE


## 如何提问-群规

1. 工作是时间严禁讨论与技术无关话题，严禁斗图。工作时间为上午 9 点到 12 点，下午 1 点半到 6 点。
2. 欢迎提问，尽量少提很具体的问题，比如：你在公司写的代码为什么不 work，然贴一段代码出来这种。
3.  多提抽象性问题，比如找出你自己对于 asp.net core 现在理解还不够深入的点来问（所以首先你要知道完全掌握 asp.net core 需要掌握哪些点）。
4.  如果你一定要给我提问题，请不要 @群里的任何人，没有人一定要帮你解决问题。
5.  可以发 issue 到 github https://github.com/yoyomooc/asp.net-core--for-beginner ,也有助于沉淀知识 ，群主尽量每天晚上去瞄一下。
6. 定时清人，3 个月没有说过话的先清除，后面怎么清我还没有想好。 回归初心，打造高质量的 asp.net core 学习群。

